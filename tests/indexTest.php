<?php
require '../vendor/autoload.php';
require '../index.php';

use Tester\Assert;
use Tester\TestCase;

class DatabaseTest extends TestCase
{
    public function testConstructor()
    {
        $db = new Database('localhost', 'root', '', 'TravelBlog');

        Assert::null($db->getConn()->connect_error);
    }

    public function testGetConn()
    {
        $db = new Database('localhost', 'root', '', 'TravelBlog');
        $conn = $db->getConn();
    
        Assert::true(is_object($conn));
    }

    public function testClose()
    {
        $db = new Database('localhost', 'root', '', 'TravelBlog');
        $db->close();

        Assert::null($db->getConn());
    }
}

class ArticleTest extends TestCase
{
    public function testConstructor()
    {
        $db = new Database('localhost', 'root', '', 'TravelBlog');
        $article = new Article($db);

        Assert::type(Article::class, $article);
    }

    public function testGetArticleById()
    {
        $db = new Database('localhost', 'root', '', 'TravelBlog');
        $article = new Article($db);

        $result = $article->getArticleById(1); // assuming that an article with ID 1 exists

        Assert::type('array', $result);
    }
}

// Run the tests
$databaseTestCase = new DatabaseTest;
$databaseTestCase->run();

$articleTestCase = new ArticleTest;
$articleTestCase->run();