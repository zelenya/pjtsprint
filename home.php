<?php
include 'logout.php';
require __DIR__ . '/vendor/autoload.php';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "TravelBlog";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Chyba připojení k databázi: " . $conn->connect_error);
}

// Načtení destinací
$destinationSql = "SELECT idDestination, Name FROM Destination";
$destinationResult = $conn->query($destinationSql);
$destinations = [];
while ($row = $destinationResult->fetch_assoc()) {
    $destinations[] = ['id' => $row["idDestination"], 'name' => $row["Name"]];
}

// Získání vybrané destinace z GET requestu
$selectedDestination = isset($_GET['destination']) ? (int) $_GET['destination'] : 0;

// SQL dotaz pro získání článků s možným filtrováním podle destinace
$sql = "SELECT Articles.idArticles, Articles.Title, Articles.Content, Articles.ProfileImg, Users.User AS Author, Articles.DatePublic, Articles.Destination
        FROM Articles
        INNER JOIN Users ON Articles.Author = Users.idUsers";
if ($selectedDestination > 0) {
    $sql .= " WHERE Articles.Destination = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $selectedDestination);
    $stmt->execute();
    $result = $stmt->get_result();
} else {
    $result = $conn->query($sql);
}

// Příprava dat pro předání do šablony
$data = [];
while ($row = $result->fetch_assoc()) {
    $data[] = [
        'id' => $row["idArticles"], // Přidáno ID pro odkazy
        'title' => $row["Title"],
        'author' => $row["Author"],
        'datePublic' => $row["DatePublic"],
        'shortContent' => substr($row["Content"], 0, 75) . "...",
        'destination' => $row["Destination"],
        'ProfileImg' => $row["ProfileImg"]
    ];
}

// Uzavření spojení s databází a případných příkazů
if (isset($stmt)) {
    $stmt->close();
}
$conn->close();

// Předání dat do Latte šablony
$latte = new Latte\Engine();
$latte->render('templates/home.latte', ['articles' => $data, 'destinations' => $destinations, 'selectedDestination' => $selectedDestination]);
?>
