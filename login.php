<?php
session_start();
require __DIR__ . '/vendor/autoload.php';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "TravelBlog";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$message = '';

// Kontrola, zda přišel uživatel po registraci
if (isset($_SESSION['just_registered'])) {
    $message = 'Registrace byla úspěšná. Nyní se můžete přihlásit.';
    unset($_SESSION['just_registered']); // Odstranění této informace ze session po zobrazení
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $stmt = $conn->prepare("SELECT Pasword FROM Users WHERE UserName = ?");
    if ($stmt === false) {
        die('prepare() failed: ' . htmlspecialchars($conn->error));
    }
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->bind_result($hashedPassword);
    $stmt->fetch();

    // Ořízněte hash hesla
    $hashedPassword = trim($hashedPassword);

    if (password_verify($password, $hashedPassword)) {
        $_SESSION['username'] = $username;
        header("Location: home.php"); // Přesměrujte na stránku po úspěšném přihlášení
        exit;
    } else {
        $message = 'Nesprávné uživatelské jméno nebo heslo.';
    }
    $stmt->close();
}
$conn->close();

$latte = new Latte\Engine;
$latte->setTempDirectory(__DIR__ . '/temp');
$params = ['message' => $message];
$latte->render(__DIR__ . '/templates/login.latte', $params);
?>
