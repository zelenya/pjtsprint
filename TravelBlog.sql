-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 14. kvě 2024, 15:50
-- Verze serveru: 10.4.32-MariaDB
-- Verze PHP: 8.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `travelblog`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE `articles` (
  `idArticles` int(11) NOT NULL,
  `Title` varchar(25) NOT NULL,
  `Content` longtext NOT NULL,
  `ProfileImg` varchar(45) DEFAULT NULL,
  `Author` int(11) NOT NULL,
  `Destination` int(11) NOT NULL,
  `DatePublic` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`idArticles`, `Title`, `Content`, `ProfileImg`, `Author`, `Destination`, `DatePublic`) VALUES
(1, 'Šumavské slatě', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque pretium lectus id turpis. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla quis diam. In enim a arcu imperdiet malesuada. Nulla pulvinar eleifend sem. Aenean placerat. Aliquam erat volutpat. In convallis. Phasellus faucibus molestie nisl. Suspendisse nisl. Nulla non lectus sed nisl molestie malesuada. Maecenas lorem. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Proin mattis lacinia justo.', 'uploadImages/slateSumava.jpg', 2, 1, '2017-06-15'),
(2, 'Materhorn v noci', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque pretium lectus id turpis. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla quis diam. In enim a arcu imperdiet malesuada. Nulla pulvinar eleifend sem. Aenean placerat. Aliquam erat volutpat. In convallis. Phasellus faucibus molestie nisl. Suspendisse nisl. Nulla non lectus sed nisl molestie malesuada. Maecenas lorem. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Proin mattis lacinia justo.', 'uploadImages/Matterhorn.jpg', 3, 5, '2017-07-15'),
(3, 'Šumava slatě II', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque pretium lectus id turpis. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla quis diam. In enim a arcu imperdiet malesuada. Nulla pulvinar eleifend sem. Aenean placerat. Aliquam erat volutpat. In convallis. Phasellus faucibus molestie nisl. Suspendisse nisl. Nulla non lectus sed nisl molestie malesuada. Maecenas lorem. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Proin mattis lacinia justo.', 'uploadImages/slateSumava.jpg', 2, 1, '2017-07-15'),
(4, 'Materhorn přes den a noc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque pretium lectus id turpis. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla quis diam. In enim a arcu imperdiet malesuada. Nulla pulvinar eleifend sem. Aenean placerat. Aliquam erat volutpat. In convallis. Phasellus faucibus molestie nisl. Suspendisse nisl. Nulla non lectus sed nisl molestie malesuada. Maecenas lorem. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Proin mattis lacinia justo.', 'uploadImages/Matterhorn.jpg', 3, 5, '2017-07-15');

-- --------------------------------------------------------

--
-- Struktura tabulky `destination`
--

CREATE TABLE `destination` (
  `idDestination` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `destination`
--

INSERT INTO `destination` (`idDestination`, `Name`) VALUES
(1, 'Česko'),
(2, 'Španělsko'),
(3, 'Norsko'),
(4, 'Švýcarsko'),
(5, 'Itálie');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL,
  `UserName` varchar(45) NOT NULL,
  `User` varchar(45) NOT NULL,
  `UserEmail` varchar(45) NOT NULL,
  `Pasword` varchar(255) NOT NULL,
  `Role` set('admin','delegate') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`idUsers`, `UserName`, `User`, `UserEmail`, `Pasword`, `Role`) VALUES
(1, 'Admin', 'Administrátor', 'admin@travelblog.cz', 'efacc4001e857f7eba4ae781c2932dedf843865e', 'admin'),
(2, 'Delegat1', 'Karel Novák', 'novak@travelblog.cz', 'bec6d274aba05aeeb195b870b6c595c44b05c086', 'delegate'),
(3, 'Delegat2', 'Jana Malá', 'mala@travelblog.cz', '8b0e26325bc7b6ff6a3c7c573dd8dd35932b23cc', 'delegate'),
(4, 'ddd', 'ddd', 'ddd@d.d', '$2y$10$E4nfGM0hPdpBx/oBC2W0TO6wPOlvnrzTsuPA2UeumSttNothS9IbG', '');

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`idArticles`),
  ADD KEY `Author_idx` (`Author`),
  ADD KEY `Destination_idx` (`Destination`);

--
-- Indexy pro tabulku `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`idDestination`);

--
-- Indexy pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUsers`),
  ADD UNIQUE KEY `UserEmail_UNIQUE` (`UserEmail`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `articles`
--
ALTER TABLE `articles`
  MODIFY `idArticles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pro tabulku `destination`
--
ALTER TABLE `destination`
  MODIFY `idDestination` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
