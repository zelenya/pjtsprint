<?php
include 'logout.php';
require 'vendor/autoload.php';

class Database {
    private $conn;

    public function __construct($servername, $username, $password, $dbname) {
        try {
            $this->conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            die("Chyba připojení k databázi: " . $e->getMessage());
        }
    }

    public function getConn() {
        return $this->conn;
    }

    public function close() {
        $this->conn = null;
    }
}

class Article {
    private $db;

    public function __construct(Database $db) {
        $this->db = $db;
    }

    public function getArticleById($id) {
        $sql = "SELECT Articles.idArticles, Articles.Title, Destination.Name AS Destination, Articles.Content, Articles.ProfileImg, 
                Users.User AS Author, Users.UserEmail, Articles.DatePublic
                FROM Articles
                INNER JOIN Destination ON Articles.Destination = Destination.idDestination
                INNER JOIN Users ON Articles.Author = Users.idUsers
                WHERE Articles.idArticles = :id";

        $stmt = $this->db->getConn()->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);  
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($result)) {
            return $result[0];
        } else {
            echo "Článek s daným ID nebyl nalezen.";
            exit;
        }
    }
}

class Template {
    public function render($template, $params) {
        $latte = new Latte\Engine;
        $latte->render($template, $params);
    }
}

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "TravelBlog";

$db = new Database($servername, $username, $password, $dbname);

$articleModel = new Article($db);
$articleId = isset($_GET['id']) ? (int)$_GET['id'] : null;

if (!$articleId) {
    die("ID článku nebylo specifikováno.");
}

$article = $articleModel->getArticleById($articleId);

$template = new Template();
$template->render('templates/article.latte', ['article' => $article]);

$db->close();
?>