<?php
require __DIR__ . '/vendor/autoload.php';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "TravelBlog";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$message = '';  // Inicializace proměnné pro zprávu, ponecháme ji prázdnou

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['username']) && isset($_POST['user']) && isset($_POST['email']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $user = $_POST['user'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    $checkUser = $conn->prepare("SELECT UserName FROM Users WHERE UserName = ? OR UserEmail = ?");
    if ($checkUser === false) {
        die('prepare() failed: ' . htmlspecialchars($conn->error));
    }
    $checkUser->bind_param("ss", $username, $email);
    $checkUser->execute();
    $checkUser->store_result();
    if ($checkUser->num_rows > 0) {
        $message = 'Uživatelské jméno nebo email již existují.';
    } else {
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        $stmt = $conn->prepare("INSERT INTO Users (UserName, User, UserEmail, Pasword) VALUES (?, ?, ?, ?)");
        if ($stmt === false) {
            die('prepare() failed: ' . htmlspecialchars($conn->error));
        }
        $stmt->bind_param("ssss", $username, $user, $email, $hashedPassword);
        if ($stmt->execute()) {
            $message = 'Uživatel byl úspěšně zaregistrován!';
            header("Location: login.php");  // Přesměrování na přihlašovací stránku
            exit;
        } else {
            $message = 'Nastala chyba při registraci: ' . $stmt->error;
        }
        $stmt->close();
    }
    $checkUser->close();
}
$conn->close();

if (empty($message)) {
    $message = '';  // Ujistěte se, že zpráva je prázdná, pokud nebyla nastavena
}

$latte = new Latte\Engine;
$latte->setTempDirectory(__DIR__ . '/temp');
$params = ['message' => $message];
$latte->render(__DIR__ . '/templates/register.latte', $params);
?>
