<?php
session_start();
require __DIR__ . '/vendor/autoload.php';

$isLoggedIn = isset($_SESSION['username']);

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['logout'])) {
    $_SESSION = array();
    session_destroy();
    header("Location: vypis.php");
    exit;
}

$latte = new Latte\Engine;
$latte->setTempDirectory(__DIR__ . '/temp');
$params = ['isLoggedIn' => $isLoggedIn];
$latte->render(__DIR__ . '/templates/logout.latte', $params);
?>