<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Projekt\pjtsprint/templates/login.latte */
final class Template88d6ee4c05 extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\Projekt\\pjtsprint/templates/login.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Přihlášení</title>
</head>
<body>
    <div class="login-form">
        <h1>Přihlášení</h1>
        <form method="post" action="">
            <label for="login_username">Uživatelské jméno:</label><br>
            <input type="text" id="login_username" name="login_username"><br>
    
            <label for="login_password">Heslo:</label><br>
            <input type="password" id="login_password" name="login_password"><br><br>
    
            <input type="submit" value="Přihlásit">
        </form>
';
		if (isset($error)) /* line 20 */ {
			echo '    <p class="error-message" style="color: red;">';
			echo LR\Filters::escapeHtmlText($error) /* line 21 */;
			echo '</p>
';
		}
		if (isset($message)) /* line 23 */ {
			echo '    <p class="success-message" style="color: green;">';
			echo LR\Filters::escapeHtmlText($message) /* line 24 */;
			echo '</p>
';
		}
		echo '    </div>
</body>
</html>
';
	}
}
