<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Projekt\pjtsprint/templates/register.latte */
final class Template18f167b371 extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\Projekt\\pjtsprint/templates/register.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>Registrace</title>
</head>
<body>
    <h1>Registrace nového uživatele</h1>
';
		if ($message != '') /* line 9 */ {
			if ($message) /* line 10 */ {
				echo '        <p style="color: ';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeCss($message == 'Uživatel byl úspěšně zaregistrován!' ? 'green' : 'red')) /* line 10 */;
				echo ';">';
				echo LR\Filters::escapeHtmlText($message) /* line 10 */;
				echo '</p>
';
			}
		}
		echo '    <form action="register.php" method="post">
        <label for="username">Uživatelské jméno:</label>
        <input type="text" id="username" name="username" required><br>
        <label for="user">Celé jméno:</label>
        <input type="text" id="user" name="user" required><br>
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br>
        <label for="password">Heslo:</label>
        <input type="password" id="password" name="password" required><br>
        <button type="submit">Registrovat</button>
    </form>
</body>
</html>
';
	}
}
