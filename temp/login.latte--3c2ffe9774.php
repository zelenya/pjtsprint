<?php

use Latte\Runtime as LR;

/** source: templates/login.latte */
final class Template3c2ffe9774 extends Latte\Runtime\Template
{
	public const Source = 'templates/login.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Přihlášení</title>
</head>
<body>
    <div class="login-form">
        <h1>Přihlášení</h1>
        <form method="POST" action="">
            <div class="form-group">
                <label for="username">Uživatelské jméno:</label>
                <input type="text" id="username" name="username" required>
            </div>
            <div class="form-group">
                <label for="password">Heslo:</label>
                <input type="password" id="password" name="password" required>
            </div>
            <div class="form-group">
                <button type="submit">Přihlásit se</button>
            </div>
        </form>
';
		if (isset($error)) /* line 24 */ {
			echo '            <p class="error-message">';
			echo LR\Filters::escapeHtmlText($error) /* line 25 */;
			echo '</p>
';
		}
		if (isset($message)) /* line 27 */ {
			echo '            <p class="success-message">';
			echo LR\Filters::escapeHtmlText($message) /* line 28 */;
			echo '</p>
';
		}
		echo '    </div>
</body>
</html>
';
	}
}
