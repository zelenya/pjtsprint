<?php

use Latte\Runtime as LR;

/** source: templates/vypis.latte */
final class Templated82b894e80 extends Latte\Runtime\Template
{
	public const Source = 'templates/vypis.latte';

	public const Blocks = [
		['content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seznam článků</title>
    <link rel="stylesheet" href="style/style.css"> <!-- Odkazuje na externí CSS soubor -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Judson:ital,wght@0,400;0,700;1,400&family=Open+Sans:ital,wght@0,300..800;1,300..800&display=swap" rel="stylesheet">
    <link href=\'https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css\' rel=\'stylesheet\'>
</head>
<body>
';
		$this->renderBlock('content', get_defined_vars()) /* line 14 */;
		echo '    <script>
        var select = document.querySelector("select");
        var chevron = document.querySelector(".chevron");
        var body = document.querySelector("body");

        select.onclick = function (event) {
            chevron.classList.toggle("active");
            event.stopPropagation();
        }

        body.onclick = function (event) {
            if (!select.contains(event.target)) {
                chevron.classList.remove("active");
            }
        }
    </script>
</body>
</html>

';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['destination' => '20', 'article' => '28'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}


	/** {block content} on line 14 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '    <section class="vypis">
        <h1>Nedávná dobrodružství</h1>
        <form action="" method="GET">
            <select id="select" onclick="onclick()" name="destination" onchange="this.form.submit()">
                <option value="">Vše</option>
';
		foreach ($destinations as $destination) /* line 20 */ {
			echo '                    <option class="destinations" value="';
			echo LR\Filters::escapeHtmlAttr($destination['id']) /* line 21 */;
			echo '"';
			$ʟ_tmp = ['selected' => $destination['id'] == $selectedDestination];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 21 */;
			echo '>';
			echo LR\Filters::escapeHtmlText($destination['name']) /* line 21 */;
			echo '</option>
';

		}

		echo '            </select>
            <img class="chevron" src="uploadImages/chevron.png">
        </form>
        <p class="popis">Podívejte se na naše nejnovější dobrodružství</p>
        <div class="vypis_clanku">
';
		foreach ($articles as $article) /* line 28 */ {
			echo '                <div class="clanek">
                    <a href="index.php?id=';
			echo LR\Filters::escapeHtmlAttr($article['id']) /* line 30 */;
			echo '">
                        <img src="';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($article['ProfileImg'])) /* line 31 */;
			echo '" alt="Profile Image" class="article-image">
                        <div class="clanek_text">
                            <h2>';
			echo LR\Filters::escapeHtmlText($article['title']) /* line 33 */;
			echo '</h2>
                            <p class="short_content">';
			echo LR\Filters::escapeHtmlText($article['shortContent']) /* line 34 */;
			echo '</p>
                            <p class="clanek_info">
                                ';
			echo LR\Filters::escapeHtmlText($article['author']) /* line 36 */;
			echo ', 
                                ';
			echo LR\Filters::escapeHtmlText($article['datePublic']) /* line 37 */;
			echo '
                            </p>
                        </div>
                    </a>
                </div>
';

		}

		echo '        </div>
    </section>
';
	}
}
