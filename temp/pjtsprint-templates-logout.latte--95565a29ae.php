<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\pjtsprint/templates/logout.latte */
final class Template95565a29ae extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\pjtsprint/templates/logout.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <div class="header">
        <img onclick="document.location=\'home.php\'" class="logo" src="uploadImages/Travel_Blog.png">
        <button class="visible_menu" onclick="document.location=\'vypis.php\'">Destinace</button>
        <div class="visible_menu">
';
		if (!$isLoggedIn) /* line 11 */ {
			echo '                <button onclick="document.location=\'register.php\'">Registrace</button>
                <button class="login" onclick="document.location=\'login.php\'">Log in</button>
';
		}
		if ($isLoggedIn) /* line 15 */ {
			echo '                <form action="logout.php" method="post">
                    <button type="submit" name="logout">Odhlásit se</button>
                </form>
';
		}
		echo '        </div>

        
        <img class="burger_menu" src="uploadImages/burger_menu.png">
        <div class="hidden_menu">
            <button class="destination" onclick="document.location=\'vypis.php\'">Destinace</button>
';
		if (!$isLoggedIn) /* line 26 */ {
			echo '                <button onclick="document.location=\'register.php\'">Registrace</button>
                <button class="login_hidden" onclick="document.location=\'login.php\'">Log in</button>
';
		}
		if ($isLoggedIn) /* line 30 */ {
			echo '                <form action="logout.php" method="post">
                    <button class="logout_hidden" type="submit" name="logout">Odhlásit se</button>
                </form>
';
		}
		echo '        </div>
    </div>
    <script>
        var menu = document.querySelector(".hidden_menu");
        var burger = document.querySelector(".burger_menu");
        var body = document.querySelector("body");

        burger.onclick = function(event) {
            menu.classList.toggle("visible");
            event.stopPropagation();

            body.onclick = function (event) {
                if (!menu.contains(event.target)) {
                    menu.classList.remove("visible");
                }
            }
        }
    </script>
</body>
</html>';
	}
}
