<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\pjtsprint/templates/register.latte */
final class Template6e9b92077c extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\pjtsprint/templates/register.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>Registrace</title>
    <link rel="stylesheet" href="style/style.css"> <!-- Odkazuje na externí CSS soubor -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Judson:ital,wght@0,400;0,700;1,400&family=Open+Sans:ital,wght@0,300..800;1,300..800&display=swap" rel="stylesheet">
    <link href=\'https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css\' rel=\'stylesheet\'>
</head>
<body>
    <section class="register_bg">
        <div class="register">
            <h1>Registrace</h1>
';
		if ($message != '') /* line 16 */ {
			if ($message) /* line 17 */ {
				echo '                <p style="color: ';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeCss($message == 'Uživatel byl úspěšně zaregistrován!' ? 'green' : 'red')) /* line 17 */;
				echo ';">';
				echo LR\Filters::escapeHtmlText($message) /* line 17 */;
				echo '</p>
';
			}
		}
		echo '            <form action="register.php" method="post">
                <input type="text" id="username" name="username" placeholder="Uživatelské jméno" required><br>
                <input type="text" id="user" name="user" placeholder="Jméno a příjmení" required><br>
                <input type="email" id="email" name="email" placeholder="Email" required><br>
                <input type="password" id="password" name="password" placeholder="Heslo" required><br>
                <button type="submit">Registrovat</button>
                <p>Máš účet?</p>
                <button onclick="document.location=\'login.php\'">Log in</button>
            </form>
        </div>
    </section>
</body>
</html>
';
	}
}
